FROM python:3.7.5-slim
RUN python -m pip install DateTime
COPY ./test/test.py /home
# The hash is meant for comenting and comands next to it will be ignored
CMD ["python", "/home/test.py"]
